# Copyright (c) 2016 - Mathieu Bridon <bochecha@daitauha.fr>
#
# This file is part of pelican-aio-planet
#
# pelican-aio-planet is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pelican-aio-planet is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with pelican-aio-planet.  If not, see <http://www.gnu.org/licenses/>.

import threading
import http.server
import socketserver
from functools import partial

import pytest


@pytest.fixture(scope="session")
def datadir(pytestconfig):
    return pytestconfig.rootdir.join("tests", "data")


class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    """The server

    .. seealso::
        https://docs.python.org/3/library/socketserver.html?highlight=threadedtcpserver#asynchronous-mixins

    """

    allow_reuse_address = True


@pytest.fixture(scope="session")
def server(datadir):
    HOST = "localhost"
    PORT = 4242
    Handler = partial(http.server.SimpleHTTPRequestHandler, directory=datadir)

    with ThreadedTCPServer((HOST, PORT), Handler) as httpd:
        # Instead of httpd.serve_forever() which blocks, we launch it in a
        # thread
        server_thread = threading.Thread(target=httpd.serve_forever)
        # Exit the server thread when the main thread terminates
        server_thread.daemon = True
        server_thread.start()

        yield f"http://{HOST}:{PORT}"

        httpd.shutdown()
