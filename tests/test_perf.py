"""Performance & integration tests

PYTHONASYNCIODEBUG=1 pytest --profile-svg --log-cli-level=10 test_perf.py

"""
import os
from pathlib import Path
from xml.etree.ElementTree import parse as parse_xml
from pytest_profiling import Profiling

from pelican.generators import PagesGenerator
from pelican.tests.support import get_context, get_settings


def create_generator(path, extra_settings):
    settings = get_settings()

    settings["PAGE_PATHS"] = ["."]  # relative to path
    settings["DEFAULT_DATE"] = (1970, 1, 1)

    settings.update(extra_settings)
    context = get_context(settings)

    generator = PagesGenerator(
        context=context,
        settings=settings,
        path=path,
        theme=settings["THEME"],
        output_path=None,
    )
    generator.generate_context()
    return generator


def read_feeds_from_opml(path):
    tree = parse_xml(path)
    feeds = {
        child.get("title"): child.get("xmlUrl")
        for child in tree.findall(".//outline")
        if child.get("xmlUrl")
    }
    return feeds


def test_perf_integration(datadir, tmpdir):
    Profiling(False)
    feeds = read_feeds_from_opml("%s/feeds.opml" % datadir)
    templatepath = Path(datadir.join("planet.md.tmpl").strpath)
    destinationpath = Path(tmpdir.join("planet.md").strpath)

    settings = {
        "CACHE_PATH": tmpdir,
        "PLANET_FEEDS": feeds,
        "PLANET_TEMPLATE": templatepath,
        "PLANET_PAGE": destinationpath,
        "PLANET_MAX_ARTICLES_PER_FEED": 2,
        "PLANET_MAX_AGE_IN_DAYS": 365,
        "PLANET_RESOLVE_REDIRECTS": True,
    }
    generator = create_generator(datadir, settings)

    if os.getenv("PYTHONASYNCIODEBUG"):
        Profiling(True)

    from pelican.plugins.aio_planet import generate
    generate(generator)

    assert destinationpath.exists()
    assert len(destinationpath.read_text()) > 0
